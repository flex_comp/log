package log

import (
	"container/list"
	"go.uber.org/zap/zapcore"
	"sync"
)

var (
	cache    = make(map[zapcore.Level]*list.List)
	mtxCache sync.RWMutex
)

type cacheItem struct {
	format string
	args   []interface{}
}

func push(lv zapcore.Level, format string, args ...interface{}) {
	mtxCache.Lock()
	defer mtxCache.Unlock()

	ls, ok := cache[lv]
	if !ok {
		ls = list.New()
		cache[lv] = ls
	}

	ls.PushBack(&cacheItem{
		format: format,
		args:   args,
	})
}

func flush(l *Log) {
	mtxCache.RLock()
	defer mtxCache.RUnlock()

	for lv, ls := range cache {
		var (
			fn        func(...interface{})
			fnWithFmt func(string, ...interface{})
		)

		switch lv {
		case zapcore.DebugLevel:
			fn = l.Debug
			fnWithFmt = l.DebugF
		case zapcore.InfoLevel:
			fn = l.Info
			fnWithFmt = l.InfoF
		case zapcore.WarnLevel:
			fn = l.Warn
			fnWithFmt = l.WarnF
		case zapcore.ErrorLevel:
			fn = l.Error
			fnWithFmt = l.ErrorF
		}

		for e := ls.Front(); e != nil; e = e.Next() {
			item := e.Value.(*cacheItem)
			if len(item.format) == 0 {
				fn(item.args...)
				continue
			}

			fnWithFmt(item.format, item.args...)
		}

	}

	cache = make(map[zapcore.Level]*list.List)
}
