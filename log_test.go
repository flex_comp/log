package log

import (
	"gitlab.com/flex_comp/comp"
	"testing"
	"time"
)

func TestLog(t *testing.T) {
	t.Run("test_log", func(t *testing.T) {
		Debug("1", " ", "2")
		DebugF("1 %s - %s", "2", "3")

		_ = comp.Init(map[string]interface{}{
			"env":      "debug",
			"log.path": "./log/test.log",
			"log.size": 50,
			"log.age":  1,
		})

		go func() {
			Debug("foo", " ", "bar")
			DebugF("format %s - %s", "foo", "bar")
			Info("foo", " ", "bar")
			InfoF("format %s - %s", "foo", "bar")
			Warn("foo", " ", "bar")
			WarnF("format %s - %s", "foo", "bar")
			Error("foo", " ", "bar")
			ErrorF("format %s - %s", "foo", "bar")
		}()

		ch := make(chan bool, 1)
		go func() {
			<-time.After(time.Second * 10)
			ch <- true
		}()

		_ = comp.Start(ch)
	})
}
