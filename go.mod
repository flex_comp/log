module gitlab.com/flex_comp/log

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	gitlab.com/flex_comp/comp v0.1.4
	gitlab.com/flex_comp/util v0.0.0-20210729132803-de11a044b5ed
	go.uber.org/zap v1.18.1
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/tools v0.1.2 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
