package log

import (
	"fmt"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/util"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"os"
	"path"
	"strings"
	"time"
)

var (
	ins *Log
)

func init() {
	ins = new(Log)
	comp.RegLogger(ins)
	_ = comp.RegComp(ins)
}

type Log struct {
	z      *zap.SugaredLogger
	stdout bool
}

func (c *Log) Init(execArgs map[string]interface{}, args ...interface{}) error {
	var (
		id       string
		env      = util.ToString(execArgs["env"])
		logPath  = util.ToString(execArgs["log.path"])
		maxSize  = util.ToInt(execArgs["log.size"])
		maxAge   = util.ToInt(execArgs["log.age"])
		debugOut = env == "debug"
	)

	c.stdout = debugOut

	if v, ok := execArgs["id"]; ok {
		id = util.ToString(v)
	} else {
		id = time.Now().Format("20060102_150405")
	}

	dir, name := path.Split(logPath)
	pureName := name
	extIdx := strings.LastIndex(name, ".")
	if extIdx >= 0 {
		pureName = name[:extIdx]
	}

	item := []string{
		env,
		pureName,
		id,
		util.ToString(os.Getpid()),
	}

	name = fmt.Sprintf("%s%s", strings.Join(item, "_"), path.Ext(logPath))

	s := zapcore.AddSync(&lumberjack.Logger{
		Filename: path.Join(dir, name),
		MaxSize:  maxSize,
		MaxAge:   maxAge,
	})

	var outLevel zapcore.Level
	if debugOut {
		outLevel = zapcore.DebugLevel
	} else {
		outLevel = zapcore.InfoLevel
	}

	en := zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		FunctionKey:    zapcore.OmitKey,
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.RFC3339TimeEncoder,
		EncodeDuration: zapcore.MillisDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}

	core := zapcore.NewCore(zapcore.NewConsoleEncoder(en), s, zap.NewAtomicLevelAt(outLevel))
	c.z = zap.New(core, zap.AddCaller(), zap.AddCallerSkip(2)).Sugar()

	flush(c)
	return nil
}

func (c *Log) Start(_ ...interface{}) error {
	return nil
}

func (c *Log) UnInit() {
	return
}

func (c *Log) Name() string {
	return "log"
}

func Debug(args ...interface{}) {
	ins.Debug(args...)
}

func (c *Log) Debug(args ...interface{}) {
	if c == nil || c.z == nil {
		push(zap.DebugLevel, "", args...)
		return
	}

	if c.stdout {
		fmt.Println(args...)
	}

	c.z.Debug(args...)
	return
}

func Info(args ...interface{}) {
	ins.Info(args...)
}

func (c *Log) Info(args ...interface{}) {
	if c == nil || c.z == nil {
		push(zap.InfoLevel, "", args...)
		return
	}

	if c.stdout {
		fmt.Println(args...)
	}

	c.z.Info(args...)
	return
}

func Warn(args ...interface{}) {
	ins.Warn(args...)
}

func (c *Log) Warn(args ...interface{}) {
	if c == nil || c.z == nil {
		push(zap.WarnLevel, "", args...)
		return
	}

	if c.stdout {
		fmt.Println(args...)
	}

	c.z.Warn(args...)
	return
}

func Error(args ...interface{}) {
	ins.Error(args...)
}

func (c *Log) Error(args ...interface{}) {
	if c == nil || c.z == nil {
		push(zap.ErrorLevel, "", args...)
		return
	}

	if c.stdout {
		fmt.Println(args...)
	}

	c.z.Error(args...)
	return
}

func DebugF(format string, args ...interface{}) {
	ins.DebugF(format, args...)
}

func (c *Log) DebugF(format string, args ...interface{}) {
	if c == nil || c.z == nil {
		push(zap.DebugLevel, format, args...)
		return
	}

	if c.stdout {
		fmt.Printf(fmt.Sprintf("%s\n", format), args...)
	}

	c.z.Debugf(format, args...)
	return
}

func InfoF(format string, args ...interface{}) {
	ins.InfoF(format, args...)
}

func (c *Log) InfoF(format string, args ...interface{}) {
	if c == nil || c.z == nil {
		push(zap.InfoLevel, format, args...)
		return
	}

	if c.stdout {
		fmt.Printf(fmt.Sprintf("%s\n", format), args...)
	}

	c.z.Infof(format, args...)
	return
}

func WarnF(format string, args ...interface{}) {
	ins.WarnF(format, args...)
}

func (c *Log) WarnF(format string, args ...interface{}) {
	if c == nil || c.z == nil {
		push(zap.WarnLevel, format, args...)
		return
	}

	if c.stdout {
		fmt.Printf(fmt.Sprintf("%s\n", format), args...)
	}

	c.z.Warnf(format, args...)
	return
}

func ErrorF(format string, args ...interface{}) {
	ins.ErrorF(format, args...)
}

func (c *Log) ErrorF(format string, args ...interface{}) {
	if c == nil || c.z == nil {
		push(zap.ErrorLevel, format, args...)
		return
	}

	if c.stdout {
		fmt.Printf(fmt.Sprintf("%s\n", format), args...)
	}

	c.z.Errorf(format, args...)
	return
}
